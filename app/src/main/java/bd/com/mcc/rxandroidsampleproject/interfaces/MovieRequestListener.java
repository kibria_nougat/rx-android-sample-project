package bd.com.mcc.rxandroidsampleproject.interfaces;

import bd.com.mcc.rxandroidsampleproject.model.MovieResponse;

public interface MovieRequestListener {
    void onRequestComplete(MovieResponse data);
    void onRequestFailed(String data);
}
