package bd.com.mcc.rxandroidsampleproject;

import android.app.Application;

public class MyApplication extends Application {

    private RxBus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        bus = new RxBus();
    }

    public MyApplication getApplication(){
        return this;
    }

    public RxBus bus() {
        return bus;
    }

}
