package bd.com.mcc.rxandroidsampleproject.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import bd.com.mcc.rxandroidsampleproject.MyApplication;
import bd.com.mcc.rxandroidsampleproject.R;
import bd.com.mcc.rxandroidsampleproject.interfaces.MovieRequestListener;
import bd.com.mcc.rxandroidsampleproject.model.MovieResponse;
import bd.com.mcc.rxandroidsampleproject.presenter.MovieListPresenter;
import bd.com.mcc.rxandroidsampleproject.view.adapter.MoviesAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity implements MovieRequestListener {

    @BindView(R.id.rvMovies)
    RecyclerView rvMovies;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private RecyclerView.Adapter adapter;
    private MovieListPresenter presenter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        progressBar.setVisibility(View.VISIBLE);
        presenter = MovieListPresenter.getPresenterInstance();
        presenter.setListener(this);
        presenter.getMovies();
    }

    @Override
    public void onRequestComplete(MovieResponse data) {
        mLayoutManager = new LinearLayoutManager(this);
        rvMovies.setLayoutManager(mLayoutManager);
        rvMovies.setItemAnimator(new DefaultItemAnimator());
        adapter = new MoviesAdapter(data.getResults(), MainActivity.this);
        rvMovies.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);

        ((MyApplication) getApplication())
                .bus()
                .getPublisher()
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        int position = (int) object;
                        Toast.makeText(MainActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void onRequestFailed(String data) {
        progressBar.setVisibility(View.GONE);
    }


}
