package bd.com.mcc.rxandroidsampleproject;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBus {
    public RxBus() {
    }

    private PublishSubject<Object> publishSubject = PublishSubject.create();

    public void send(Object o) {
        publishSubject.onNext(o);
    }

    public Observable<Object> getPublisher() {
        return publishSubject;
    }

}
