package bd.com.mcc.rxandroidsampleproject.presenter;


import bd.com.mcc.rxandroidsampleproject.interfaces.MovieRequestListener;
import bd.com.mcc.rxandroidsampleproject.model.MovieResponse;
import bd.com.mcc.rxandroidsampleproject.networking.ApiClient;
import bd.com.mcc.rxandroidsampleproject.networking.NetworkInterface;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MovieListPresenter {

    public static MovieListPresenter instance;
    public MovieRequestListener listener;


    private static final String API_KEY = "9a4176b10acbfa14bf4740762a7a4ecb";

    private MovieListPresenter() {
    }

    public static MovieListPresenter getPresenterInstance() {
        if (instance == null) {
            instance = new MovieListPresenter();
        }
        return instance;
    }

    public void setListener(MovieRequestListener listener) {
        this.listener = listener;
    }

    public Observable<MovieResponse> getObservable() {
        return ApiClient.getRetrofit()
                .create(NetworkInterface.class)
                .getMovies(API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<MovieResponse> getObserver() {
        return new DisposableObserver<MovieResponse>() {
            @Override
            public void onNext(MovieResponse movieResponse) {
                listener.onRequestComplete(movieResponse);
            }

            @Override
            public void onError(Throwable e) {
                listener.onRequestFailed(e.toString());
            }

            @Override
            public void onComplete() {
            }
        };
    }

    public void getMovies() {
        getObservable().subscribe(getObserver());
    }

}
